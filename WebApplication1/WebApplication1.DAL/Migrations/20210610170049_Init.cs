﻿using Microsoft.EntityFrameworkCore.Migrations;

<<<<<<< HEAD:WebApplication1/WebApplication1.DAL/Migrations/20210610170049_Init.cs
namespace WebApplication1.DAL.Migrations
=======
namespace WebApplication6.DAL.Migrations
>>>>>>> 3ebfe22b54cf063a3ad8cb71dd4a8e5d38b6f52c:WebApplication6/WebApplication6.DAL/Migrations/20210610170049_Init.cs
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "NVARCHAR(50)", nullable: true),
                    LastName = table.Column<string>(type: "NVARCHAR(50)", nullable: true),
                    Age = table.Column<int>(type: "INTEGER", nullable: false),
                    Country = table.Column<string>(type: "NVARCHAR(50)", nullable: true),
                    City = table.Column<string>(type: "NVARCHAR(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Customers");
        }
    }
}

