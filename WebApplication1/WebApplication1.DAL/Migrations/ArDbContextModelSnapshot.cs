﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;

namespace WebApplication1.DAL.Migrations
{
    [DbContext(typeof(ArDbContext))]
    partial class ArDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.7")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("WebApplication2.Core.Entities.Student", b =>
            {
                b.Property<int>("Id")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("int")
                    .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                b.Property<int>("Age")
                    .HasColumnType("INTEGER");

                b.Property<string>("City")
                    .HasColumnType("NVARCHAR(50)");

                b.Property<string>("Country")
                    .HasColumnType("NVARCHAR(50)");

                b.Property<string>("FirstName")
                    .HasColumnType("NVARCHAR(50)");

                b.Property<string>("LastName")
                    .HasColumnType("NVARCHAR(50)");

                b.HasKey("Id");

                b.ToTable("Customers");
            });

            modelBuilder.Entity("WebApplication2.Core.Entities.University", b =>
            {
                b.Property<int>("Id")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("int")
                    .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                b.Property<string>("Name")
                    .HasColumnType("NVARCHAR(100)");

                b.HasKey("Id");

                b.ToTable("Universities");
            });

            modelBuilder.Entity("WebApplication2.Core.Entities.User", b =>
            {
                b.Property<int>("Id")
                    .ValueGeneratedOnAdd()
                    .HasColumnType("int")
                    .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                b.Property<string>("Password")
                    .HasColumnType("NVARCHAR(100)");

                b.Property<string>("Username")
                    .HasColumnType("NVARCHAR(100)");

                b.HasKey("Id");

                b.HasIndex("Username")
                    .IsUnique()
                    .HasFilter("[Username] IS NOT NULL");

                b.ToTable("Users");
            });
#pragma warning restore 612, 618
        }
    }
}