﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using WebApplication1.Core.Abstractions;
using WebApplication1.Core.Abstractions.Repositories;
using WebApplication1.DAL.Repositories;

namespace WebApplication1.DAL
{
    public class RepositoryManager : IRepositoryManager
    {
        private readonly ArDbContext _dbContext;

        public RepositoryManager(ArDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        private ICustomerRepository _customers;
        public ICustomerRepository Customers => _customers ?? (_customers = new CustomerRepository(_dbContext));

        private IUniversityRepository _universities;
        public IUniversityRepository Universities => _universities ?? (_universities = new UniversityRepository(_dbContext));

        private IUserRepository _users;
        public IUserRepository Users => _users ?? (_users = new UserRepository(_dbContext));

        public int SaveChanges()
        {
            return _dbContext.SaveChanges();
        }

        public Task<int> SaveChangesAsync()
        {
            return _dbContext.SaveChangesAsync();
        }

        public ISqlTransaction BeginTransaction(IsolationLevel isolation = IsolationLevel.ReadCommitted)
        {
            return SqlTransaction.Begin(_dbContext, isolation);
        }
    }
}
