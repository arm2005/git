﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication1.Core.Abstractions.Repositories;
using WebApplication1.Core.Entities;

namespace WebApplication1.DAL.Repositories
{
    public class UserRepository : SqlRepositoryBase<User>, IUserRepository
    {
        public UserRepository(ArDbContext dbContext)
            : base(dbContext)
        {
        }
    }
}