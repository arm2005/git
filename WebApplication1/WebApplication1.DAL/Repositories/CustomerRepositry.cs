﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication1.Core.Abstractions.Repositories;
using WebApplication1.Core.Entities;
using WebApplication1.DAL.Repositories;

namespace WebApplication1.DAL.Repositories
{
    public class CustomerRepository : SqlRepositoryBase<Customer>, ICustomerRepository
    {
        public CustomerRepository(ArDbContext dbContext)
            : base(dbContext)
        {

        }
    }
}