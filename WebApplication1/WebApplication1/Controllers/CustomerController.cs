﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Core.Abstractions.Operations;
using WebApplication1.Core.Entities;
using WebApplication1.Core.Models;

namespace WebApplication6.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StudentsController : ControllerBase
    {
        private readonly ICustomerBL _customerBL;

        public StudentsController(ICustomerBL customerBL)
        {
            _customerBL = customerBL;
        }

        [HttpPost]
        public IActionResult AddStudent([FromBody] CreateCustomerModel customerModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var addedCustomer = _customerBL.AddCustomer(customerModel);

            return Created("", addedCustomer);
        }

        [HttpGet]
        [Authorize]
        public IActionResult GetCustomers([FromQuery] CustomerFilterModel filterModel)
        {
            var customers = _customerBL.GetCustomers(filterModel);

            return Ok(customers);
        }

        [HttpDelete("{id}")]
        public IActionResult RemoveCustomer([FromRoute] int id)
        {
            _customerBL.RemoveCustomer(id);

            return Ok();
        }

        [HttpPut("{id}")]
        public IActionResult EditCustomer([FromRoute] int id, [FromBody] Customer customer)
        {
            _customerBL.EditCustomer(id, customer);

            return Ok();
        }

        [HttpPost("test")]
        public async Task<IActionResult> TestTransactionAsync()
        {
            await _customerBL.TestTransactionAsync();
            return Ok();
        }
    }
}
