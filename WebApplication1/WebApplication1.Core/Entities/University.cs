﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication1.Core.Entities
{
    public class University
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
