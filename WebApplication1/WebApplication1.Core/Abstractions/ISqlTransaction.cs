﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication1.Core.Abstractions
{
    public interface ISqlTransaction : IDisposable
    {
        void Commit();
        void Rollback();
    }
}

