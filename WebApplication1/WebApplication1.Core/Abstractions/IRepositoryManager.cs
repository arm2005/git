﻿using System.Data;
using System.Threading.Tasks;
using WebApplication1.Core.Abstractions.Repositories;

namespace WebApplication1.Core.Abstractions
{
    public interface IRepositoryManager
    {
        ICustomerRepository Customers { get; }
        IUniversityRepository Universities { get; }
        IUserRepository Users { get; }

        int SaveChanges();
        Task<int> SaveChangesAsync();

        ISqlTransaction BeginTransaction(IsolationLevel isolation = IsolationLevel.ReadCommitted);
    }
}
