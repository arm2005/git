﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebApplication1.Core.Entities;
using WebApplication1.Core.Models;

namespace WebApplication1.Core.Abstractions.Operations
{
    public interface IUserBL
    {
        Task<UserViewModel> RegisterAsync(UserRegisterModel registerModel, HttpContext httpContext);
        Task LoginAsync(UserLoginModel loginModel, HttpContext httpContext);
        Task LogOutAsync(HttpContext httpContext);
    }
}
