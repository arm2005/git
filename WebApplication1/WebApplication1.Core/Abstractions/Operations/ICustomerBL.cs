﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebApplication1.Core.Entities;
using WebApplication1.Core.Models;

namespace WebApplication1.Core.Abstractions.Operations
{
    public interface ICustomerBL
    {
        IEnumerable<Customer> GetCustomers(CustomerFilterModel filterModel);
        void RemoveCustomer(int id);
        void EditCustomer(int id, Customer customer);
        Customer AddCustomer(CreateCustomerModel customerModel);

        Task TestTransactionAsync();
    }
}