﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication1.Core.Entities;

namespace WebApplication1.Core.Abstractions.Repositories
{
    public interface IUniversityRepository : ISqlRepository<University>
    {
    }
}
