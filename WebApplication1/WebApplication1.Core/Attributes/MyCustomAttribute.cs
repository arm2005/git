﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Core.Entities;

namespace WebApplication1.Core.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class MyCustomAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value is Customer s)
            {
                return s.City != "Moscow";
            }

            return true;
        }
    }
}
