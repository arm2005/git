﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication1.Core.Abstractions;
using WebApplication1.Core.Abstractions.Operations;
using WebApplication1.Core.Entities;
using WebApplication1.Core.Exceptions;
using WebApplication1.Core.Models;

namespace WebApplication1.BLL.Operations
{
    public class CustomerBL : ICustomerBL
    {
        private readonly IRepositoryManager _repositories;
        private readonly ILogger<CustomerBL> _logger;

        public CustomerBL(IRepositoryManager repositories, ILogger<CustomerBL> logger)
        {
            _logger = logger;
            _repositories = repositories;
        }

        public Customer AddCustomer(CreateCustomerModel customerModel)
        {
            var customer = new Customer
            {
                FirstName = customerModel.FirstName,
                LastName = customerModel.LastName,
                Age = customerModel.Age,
                City = customerModel.City,
                Country = customerModel.Country
            };

            _repositories.Customers.Add(customer);
            _repositories.SaveChanges();
            return customer;
        }

        public void EditCustomer(int id, Customer customer)
        {
            _logger.LogInformation("EditCustomer method started");
            var dbCustomer = _repositories.Customers.Get(id);

            if (dbCustomer == null)
            {
                throw new LogicException("Wrong Customer Id");
            }

            dbCustomer.FirstName = customer.FirstName;
            dbCustomer.LastName = customer.LastName;
            dbCustomer.Age = customer.Age;
            dbCustomer.City = customer.City;
            dbCustomer.Country = customer.Country;

            _repositories.Customers.Edit(dbCustomer);
            _repositories.SaveChanges();

            _logger.LogInformation("EditCustomer method finished");
        }

        public IEnumerable<Customer> GetCustomers(CustomerFilterModel filterModel)
        {

            List<Customer> list;
            if (!string.IsNullOrEmpty(filterModel.FirstName))
            {
                list = _repositories.Customers.GetWhere(x => x.FirstName.ToUpper().Contains(filterModel.FirstName.ToUpper())).ToList();
            }
            else
            {
                list = _repositories.Customers.GetWhere(x => true).ToList();
            }

            return list;
        }

        public void RemoveCustomer(int id)
        {
            var dbCustomer = _repositories.Customers.Get(id);

            if (dbCustomer == null)
            {
                throw new LogicException("Wrong Customer Id");
            }

            _repositories.Customers.Remove(dbCustomer);
            _repositories.SaveChanges();

        }

        public async Task TestTransactionAsync()
        {
            using (var transaction = _repositories.BeginTransaction())
            {
                try
                {
                    var customer = new Customer
                    {
                        FirstName = "Arman",
                        LastName = "Nurbekyan",
                        Country = "Armenia",
                        City = "Yerevan"
                    };

                    var university = new University
                    {
                        Name = "Yerevan State University"
                    };

                    _repositories.Customers.Add(customer);
                    // throw new Exception();
                    _repositories.Universities.Add(university);

                    await _repositories.SaveChangesAsync();

                    transaction.Commit();
                }
                catch (Exception)
                {
                    transaction.Rollback();
                    throw;
                }
            }
        }
    }
}